import React, { useState } from 'react';
import styled from 'styled-components'

// I think oyu have to store join? and store parent nodes in our double linked list
// maybe not its a child of the parent?? but there's only one so you can just find it inthe children?

// it also depends on how we think of join and who the parent is maybe it depends on if its a parallel or switch?

 const blockId = () => `blo_${Math.random()}`

const WORKFLOW_DATA_1 = {
  type: 'trigger',
  name: 'inquiry.completed',
  id: blockId(),
  children: []
}

const WORKFLOW_DATA = {
  type: 'trigger',
  name: 'inquiry.completed',
  id: blockId(),
  children: [{
    type: 'action',
    name: 'action 1',
    id: blockId(),
    children: [{
      type: 'parallel',
      id: blockId(),
      children: [
        {
          type: 'action',
          name: 'action 2',
          id: blockId(),
          children: [{
            type: 'action',
            name: 'action',
            id: blockId(),
            children: [{
              type: 'parallel',
              id: blockId(),
              children: [
                {
                  type: 'action',
                  name: 'action',
                  id: blockId(),
                  children: [],
                },
                {
                  type: 'action',
                  name: 'action',
                  id: blockId(),
                  children: [],
                }
              ]
            }],
          }],
        },
        {
          type: 'action',
          name: 'action 3',
          id: blockId(),
          children: [],
        },
        {
          type: 'join',
          children: [{
            type: 'action',
            name: 'action',
            id: blockId(),
            children: [],
          }]
        }
      ]

    }]
  }]
}

const Block = styled.div`
  border: 1px solid black;
  padding: 8px;
  margin-bottom: 16px;
  width: 200px;
`

const Path = styled.div`
  padding: 8px;
  margin-bottom: 16px;
  display: flex;
  flex-direction: column;
  align-items: center;
  border: 1px dashed grey;
  // &:hover {
    // border: 1px dashed grey;
    // background: rgba(0,0,0,.1);
  // }
`

const ParallelChildren = styled.div`
  display: flex;
`


const TriggerBlock = ({ block, addBlock }) => {
  const child = block.children.length > 0 && renderBlockType(block.children[0], addBlock);

  return (
    <Path>
      <Block>
        <div>{block.name}</div>
        <div>Trigger</div>
      </Block>
      <AddBlock handleClick={() => addBlock(block)}/>
      <svg>
        <path></path>
      </svg>
      <div>
        {child}
      </div>
    </Path>
  )
}

const ActionBlock = ({ block, addBlock }) => {
  const child = block.children.length > 0 && renderBlockType(block.children[0], addBlock);
  return (
    <Path>
      <Block>
        <div>{block.name}</div>
        <div>Action</div>
      </Block>
      <AddBlock handleClick={() => addBlock(block)}/>
      <div>
        {child}
      </div>
    </Path>
  )
}

const WaitBlock = ({ block, addBlock }) => {
  const child = block.children.length > 0 && renderBlockType(block.children[0], addBlock);
  return (
    <Path>
      <Block>
        <div>{block.name}</div>
        <div>Wait</div>
      </Block>
      <AddBlock handleClick={() => addBlock(block)}/>
      <div>
        {child}
      </div>
    </Path>
  )
}


//  Will need all kinds of add blocks here
const ParallelBlock = ({ block, addBlock }) => {
  const directChildren = block.children.filter(b => b.type !== 'join').map(b => renderBlockType(b, addBlock));

  const joinChildren = block.children.filter(b => b.type === 'join');

  const join = joinChildren.length > 0 ? renderBlockType(joinChildren[0], addBlock) : null

    return (
    <Path>
      <Block>Parallel</Block>
      <AddBlock handleClick={() => addBlock(block)}/>
      <ParallelChildren>{directChildren}</ParallelChildren>
      <div>{join}</div>
    </Path>
  )
}

const SwitchBlock = ({ block, addBlock }) => {
  const directChildren = block.children.filter(b => b.type !== 'join').map(b => renderBlockType(b, addBlock));

  const joinChildren = block.children.filter(b => b.type === 'join');

  const join = joinChildren.length > 0 ? renderBlockType(joinChildren[0], addBlock) : null

    return (
    <Path>
      <Block>Switch</Block>
      <AddBlock handleClick={() => addBlock(block)}/>
      <ParallelChildren>{directChildren}</ParallelChildren>
      <div>{join}</div>
    </Path>
  )
}

const JoinBlock = ({ block, addBlock }) => {
  // its not creating a new path
  const child = block.child && renderBlockType(block.child, addBlock)

  return (
    <div>
      <Block>Join</Block>
      <AddBlock handleClick={() => addBlock(block)}/>
      <div>{child}</div>
    </div>
  )
}

const renderBlockType = (block, addCurrentBlock) => {
  switch (block.type) {
    case 'trigger':
      return <TriggerBlock block={block} addBlock={addCurrentBlock} key={Math.random()} />
    case 'action':
      return <ActionBlock block={block} addBlock={addCurrentBlock} key={Math.random()}/>
    case 'wait':
      return <WaitBlock block={block} addBlock={addCurrentBlock} key={Math.random()}/>
    case 'parallel':
      return <ParallelBlock block={block} addBlock={addCurrentBlock} key={Math.random()}/>
    case 'switch':
      return <SwitchBlock block={block} addBlock={addCurrentBlock} key={Math.random()}/>
    case 'join':
      return <JoinBlock block={block} addBlock={addCurrentBlock} key={Math.random()}/>
    default:
      throw Error('unknown block type');
  }
}

const AddBlock = ({ handleClick} ) => {
  return (
    <button onClick={handleClick}>ADD</button>
  )
}



const Menu = styled.div`
  paddgin: 32px;
  border: 1px solid black;
  display: flex;
  flex-direction: column;
`

const BlockMenu = ({ currentBlock, setCurrentBlock }) => {
  const blocks = [
    'trigger',
    'action',
    'wait',
    'parallel',
    'switch',
    'join',
  ]

  const menuItems = blocks.map((block,i) => {
    return (
      <button key={i} onClick={() => setCurrentBlock(block)}>{block}</button>
    )
  })

  return (
    <div>
      <h3>Blocks</h3>
      <div>Current Block {currentBlock} </div>

      <Menu>
        {menuItems}
      </Menu>
    </div>
  )

}


const AppContainer = styled.div`
  display flex;
  justify-content: space-between;
`

const WorkflowTreeContainer = styled.div`

`

const WorkflowTree = ({ workflowData, addCurrentBlock }) => {
  const workflowTree = renderBlockType(workflowData, addCurrentBlock);

  return (
    <WorkflowTreeContainer>{workflowTree}</WorkflowTreeContainer>
  )
}

const newBlockData = (newBlockType) => {
  return {
    type: newBlockType,
    name: newBlockType,
    id: blockId(),
    children: [],
  }
}

// lol this mutates
const addChild = (newBlockType, parentBlock) => {
  switch (parentBlock.type) {
    case 'action':
    case 'trigger':
    case 'wait':
    case 'join': {

      const children = parentBlock.children;
      const newBlock = newBlockData(newBlockType);
      newBlock.children = children;
      parentBlock.children = [newBlock];
      break;
    }
    case 'parallel':
    case 'switch': {

      const newBlock = newBlockData(newBlockType);
      parentBlock.children = [...parentBlock.children, newBlock];
      break;
    }
    default:
      break;
  }

}

const Scaled = styled.div`
  transform: ${({ scaleFactor }) => `scale(${scaleFactor}, ${scaleFactor})`};
  transform-origin: left top;
`
const WorkflowSection = styled.section`
  overflow: scroll;
  height: 100vh;
  width: 80vw;
`

function App() {
  const [currentBlock, setCurrentBlock] = useState('action');
  const [workflowData, setWorkflowData] = useState(WORKFLOW_DATA);

  const [scale, setScale] = useState(1.0);
  const addCurrentBlock = (parentBlock) => {
    addChild(currentBlock, parentBlock);
    setWorkflowData(Object.assign({}, workflowData));
  }

  // How tray does this
  // transform: scale(0.9, 0.9); transform-origin: left top; width: 1864px; height: 2108px;
  return (
    <AppContainer>
      <WorkflowSection>
        <Scaled scaleFactor={scale}>
          <WorkflowTree workflowData={workflowData} addCurrentBlock={addCurrentBlock} />
        </Scaled>e .
      </WorkflowSection>
      <section>
        <BlockMenu currentBlock={currentBlock} setCurrentBlock={setCurrentBlock} />

        <label>
          {scale}
        <input type='range'  onChange={({target}) => setScale(target.value) } min="0.1" max="2.0" step="0.1"/>
        </label>
      </section>
    </AppContainer>
  );
}

export default App;
